package examen;
import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Ex2 extends JFrame {
    JTextField fisier;
    JTextField elementefisier;

    Ex2() {
        this.setSize(500, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(3, 1));
        this.fisier = new JTextField();
        fisier.setSize(100, 20);
        this.add(fisier);

        this.elementefisier=new JTextField();
        elementefisier.setSize(100,20);
        this.add(elementefisier);


        JButton button=new JButton("Write");
        button.setSize(100,20);
        button.addActionListener(e->{
            String path=fisier.getText();
            File file=new File(path);
            try {
                BufferedWriter bufferedWriter=new BufferedWriter(new FileWriter(file, true));
                String a=elementefisier.getText();
                bufferedWriter.append(a+"\n");
                bufferedWriter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        this.add(button);

        this.setVisible(true);
    }


    public static void main(String[] args) {
        new Ex2();

    }
}
